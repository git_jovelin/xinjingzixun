import logging
from logging.handlers import RotatingFileHandler

import redis

from flask import Flask, g, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect, generate_csrf
from flask_session import Session
from config import config_dict
from info.utils.common import do_index_class, user_login_data


def setup_log(config_name):
    """配置日志"""

    # 设置日志的记录等级
    logging.basicConfig(level=config_dict[config_name].LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler("logs/log", maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)


# 创建空的 app 数据库，暴露给外界调用
db = SQLAlchemy()
redis_store = None  # type: redis.StrictRedis


def create_app(config_name):
    """
        工厂方法：
        生产 app 对象
        config_name：development ——> DevelopmentConfig
        config_name：production ——> ProductionConfig
    """

    # 配置项目日志
    setup_log(config_name)

    # 创建
    app = Flask(__name__)

    # 模式
    config_class = config_dict[config_name]

    # 配置模式信息
    app.config.from_object(config_class)

    # 创建数据库
    # 懒加载 延迟加载
    db.init_app(app)

    # 创建 redis 对象
    global redis_store
    redis_store = redis.StrictRedis(
        host=config_class.REDIS_HOST,
        port=config_class.REDIS_PORT,
        db=config_class.REDIS_NUM,
        decode_responses=True)

    # 开启 flask 后端 csrf 保护
    csrf = CSRFProtect(app)

    @app.after_request
    def after_request(response):
        # 调用函数生成 csrf_token
        csrf_token = generate_csrf()
        # 通过 cookie 将值传给前端
        response.set_cookie('csrf_token', csrf_token)
        return response

    @app.errorhandler(404)
    @user_login_data
    def page_not_found(_):
        from info.models import User
        user = g.user  # type: User
        data = {
            'user_info': user.to_dict() if user else None
        }
        return render_template('news/404.html', data=data)

    # 设置 session 保存位置
    Session(app)

    # 延迟导入，真正使用时才调用
    from info.modules.index import index_bp
    from info.modules.passport import passport_bp
    from info.modules.news import news_bp
    from info.modules.profile import profile_bp
    from info.modules.admin import admin_bp

    # 注册蓝图
    app.register_blueprint(index_bp)
    app.register_blueprint(passport_bp, url_prefix='/passport')
    app.register_blueprint(news_bp, url_prefix='/news')
    app.register_blueprint(profile_bp, url_prefix='/user')
    app.register_blueprint(admin_bp, url_prefix='/admin')

    # 添加自定义过滤器
    app.add_template_filter(do_index_class, 'indexClass')

    return app
