from werkzeug.utils import redirect
from flask import render_template, g, request, jsonify, current_app, session, abort

from info import db, constants
from info.models import User, Category, News
from info.utils.image_storage import storage
from info.utils.response_code import RET
from . import *
from info.utils.common import user_login_data


@profile_bp.route('/info')
@user_login_data
def get_user_info():
    """
    获取用户信息
    1. 获取到当前登录的用户模型
    2. 返回模型中指定内容
    :return:
    """

    user = g.user
    if not user:
        # 用户未登录，重定向到主页
        return redirect('/')

    data = {
        'user_info': user.to_dict()
    }

    return render_template('news/user.html', data=data)


@profile_bp.route('/base_info', methods=['POST', 'GET'])
@user_login_data
def base_info():
    """
    用户基本信息
    :return:
    """
    user = g.user  # type: User

    if request.method == 'GET':
        data = {
            'user_info': user.to_dict()
        }
        return render_template('news/user_base_info.html', data=data)

    params = request.json
    nick_name = params.get('nick_name')
    signature = params.get('signature')
    gender = params.get('gender')

    if not all([nick_name, signature, gender]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    if gender not in ('MAN', 'WOMAN'):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    user.nick_name = nick_name
    user.signature = signature
    user.gender = gender

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg='保存数据失败')

    # 将 session 中保存的数据进行实时更新
    session['nick_name'] = nick_name

    return jsonify(errno=RET.OK, errmsg='更新成功')


@profile_bp.route('/pic_info', methods=['POST', 'GET'])
@user_login_data
def pic_info():
    user = g.user
    if request.method == 'GET':
        return render_template('news/user_pic_info.html', data={'user_info': user.to_dict()})

    # 1. 获取到上传的文件
    try:
        avatar_file = request.files.get('avatar').read()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.IOERR, errmsg='OK')

    # 2. 再将文件上传到七牛云
    try:
        url = storage(avatar_file)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg='上传图片错误')

    # 3. 将头像信息更新到当前用户的模型中

    user.avatar_url = url
    # 将数据保存到数据库
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg='保存用户数据错误')

    # 4. 返回上传的结果<avatar_url>
    return jsonify(errno=RET.OK, errmsg='OK', data={'avatar_url': constants.QINIU_DOMIN_PREFIX + url})


@profile_bp.route('/pass_info', methods=['GET', 'POST'])
@user_login_data
def pass_info():
    if request.method == 'GET':
        return render_template('news/user_pass_info.html')

    params = request.json
    old_password = params.get('old_password')
    new_password = params.get('new_password')

    if not all([old_password, new_password]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数有误')

    user = g.user  # type: User

    if not user.check_passowrd(old_password):
        return jsonify(errno=RET.PWDERR, errmsg='原密码错误')

    # 更新数据
    user.password = new_password
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg='保存数据失败')

    return jsonify(errno=RET.OK, errmsg='保存成功')


@profile_bp.route('/collection')
@user_login_data
def user_collection():
    # 获取页数
    p = request.args.get('p', 1)
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    user = g.user  # type: User
    collections = []
    current_page = 1
    total_page = 1
    try:
        # 进行分页数据查询
        paginate = user.collection_news.paginate(p, constants.USER_COLLECTION_MAX_NEWS, False)
        # 获取分页数据
        collections = paginate.items
        # 获取当前页
        current_page = paginate.page
        # 获取总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    # 收藏列表
    collection_dict_li = []
    for news in collections:
        collection_dict_li.append(news.to_basic_dict())

    data = {
        'total_page': total_page,
        'current_page': current_page,
        'collections': collection_dict_li
    }

    return render_template('news/user_collection.html', data=data)


@profile_bp.route('/news_release', methods=['GET', 'POST'])
@user_login_data
def news_release():
    user = g.user  # type: User

    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')

    if request.method == 'GET':
        categories = []
        try:
            # 获取所有的分类数据
            categories = Category.query.all()
        except Exception as e:
            current_app.logger.error(e)

        # 定义列表保存分类数据
        categories_dicts = []

        for category in categories:
            # 获取字典
            cate_dict = category.to_dict()
            # 拼接内容
            categories_dicts.append(cate_dict)

        # 移除 最新 分类
        categories_dicts.pop(0)

        current_app.logger.debug('DEBUG message with level debug ')
        current_app.logger.info('INFO message with level info')
        current_app.logger.warn('WARNING message with level warning')
        current_app.logger.error('ERROR message with level error')
        current_app.logger.critical('CRITICAL message with level critical')

        return render_template('news/user_news_release.html', data={'categories': categories_dicts})

    # POST 提交，执行发布新闻操作

    title = request.form.get('title')
    source = '个人发布'
    digest = request.form.get('digest')
    content = request.form.get('content')
    index_image = request.files.get('index_image')
    category_id = request.form.get('category_id')

    # 1.1 判断数据是否有值
    if not all([title, source, digest, content, index_image, category_id]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数有误')

    # 1.2 读取图片
    try:
        index_image = index_image.read()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='图片参数有误')

    # 2. 将标题图片上传到七牛
    try:
        key = storage(index_image)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.THIRDERR, errmsg='上传图片错误')

    # 3. 初始化新闻模型，并设置相关数据
    news = News()
    news.title = title
    news.digest = digest
    news.source = source
    news.content = content
    news.index_image_url = constants.QINIU_DOMIN_PREFIX + key
    news.category_id = category_id
    news.user_id = user.id
    # 1 代表待审核状态
    news.status = 1

    # 4. 保存到数据库
    try:
        db.session.add(news)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg='保存数据失败')

    # 5. 返回结果
    return jsonify(errno=RET.OK, errmsg='发布成功，等待审核')


@profile_bp.route('/news_list')
@user_login_data
def news_list():
    # 获取页数
    p = request.args.get('p', 1)
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    user = g.user  # type: User
    news_li = []
    current_page = 1
    total_page = 1
    try:
        paginate = News.query.filter(News.user_id == user.id).paginate(p, constants.USER_COLLECTION_MAX_NEWS, False)
        # 获取当前页数据
        news_li = paginate.items
        # 获取当前页
        current_page = paginate.page
        # 获取总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    news_dict_li = []

    for news_item in news_li:  # type: News
        news_dict_li.append(news_item.to_review_dict())

    data = {
        'news_list': news_dict_li,
        'total_page': total_page,
        'current_page': current_page
    }

    return render_template('news/user_news_list.html', data=data)


@profile_bp.route('/user_follow')
@user_login_data
def user_follow():
    # 获取页数
    p = request.args.get('p', 1)
    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        p = 1

    user = g.user  # type: User

    follows = []
    current_page = 1
    total_page = 1
    try:
        paginate = user.followed.paginate(p, constants.USER_FOLLOWED_MAX_COUNT, False)
        # 获取当前页数据
        follows = paginate.items
        # 获取当前页
        current_page = paginate.page
        # 获取总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)

    user_dict_li = []

    for follow_user in follows:  # type: User
        user_dict_li.append(follow_user.to_dict())

    data = {
        'users': user_dict_li,
        'total_page': total_page,
        'current_page': current_page
    }

    return render_template('news/user_follow.html', data=data)


@profile_bp.route('/other_info')
@user_login_data
def other_info():
    """查看其他用户信息"""
    user = g.user  # type: User

    # 获取其他用户 id
    user_id = request.args.get('id')
    if not user_id:
        abort(404)

    # 查询用户模型
    other = None  # type: User
    try:
        other = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)

    if not other:
        abort(404)

    # 判断当前登录用户是否关注过该用户
    is_followed = False
    if user:
        if other.followers.filter(User.id == user.id).count() > 0:
            is_followed = True

    # 组织数据，并返回
    data = {
        'user_info': user.to_dict(),
        'other_info': other.to_dict(),
        'is_followed': is_followed
    }

    return render_template('news/other.html', data=data)


@profile_bp.route('/other_news_list')
def other_news_list():
    # 获取页数
    p = request.args.get('p', 1)
    user_id = request.args.get('user_id')

    try:
        p = int(p)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    if not all([p, user_id]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    try:
        user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据查询错误')

    if not user:
        return jsonify(errno=RET.NODATA, errmsg='用户不存在')

    try:
        paginate = News.query.filter(News.user_id == user_id).paginate(p, constants.OTHER_NEWS_PAGE_MAX_COUNT, False)
        # 获取当前页数据
        news_li = paginate.items
        # 获取当前页
        current_page = paginate.page
        # 获取总页数
        total_page = paginate.pages
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg="数据查询错误")

    news_dict_li = []

    for news_item in news_li:  # type: News
        news_dict_li.append(news_item.to_dict())

    data = {
        'news_list': news_dict_li,
        'current_page': current_page,
        'total_page': total_page
    }

    return jsonify(errno=RET.OK, errmsg='OK', data=data)
