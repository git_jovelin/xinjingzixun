from flask import Blueprint, request, url_for, session, redirect

admin_bp = Blueprint('admin', __name__)


@admin_bp.before_request
def before_request():
    """请求勾子函数"""
    # 判断如果不是登录页面的请求
    if not request.url.endswith(url_for('admin.admin_login')):
        user_id = session.get('user_id', None)
        is_admin = session.get('is_admin', False)

        # 判断当前是否有管理员用户登录
        if not user_id or not is_admin:
            # 没有管理员用户登录，直接重定向到项目主页
            return redirect('/')


from . import views
