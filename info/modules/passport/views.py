import json
from datetime import datetime

import random

import re
from flask import request, current_app, make_response, jsonify, session
from flask_wtf.csrf import generate_csrf

from info import redis_store, constants, db
from info.lib.yuntongxun.sms import CCP
from info.models import User
from info.utils.captcha.captcha import captcha
from info.utils.response_code import RET
from . import passport_bp


@passport_bp.route('/image_code')
def get_image_code():
    """获取图片验证码"""

    # 1. 获取到当前的图片编号 id
    code_id = request.args.get('code_id')

    # 2. 验证数据
    if not code_id:
        return make_response(jsonify(
            errno=RET.NODATA,
            errmsg='无数据'))

    # 3. 生成验证码
    name, text, image = captcha.generate_captcha()
    try:
        # 保存当前生成的图片验证码内容
        redis_store.setex('ImageCode_' + code_id,
                          constants.IMAGE_CODE_REDIS_EXPIRES, text)
    except Exception as e:
        current_app.logger.error(e)
        return make_response(jsonify(
            errno=RET.DATAERR,
            errmsg='保存图片验证码失败'))

    # 4. 返回响应内容e
    resp = make_response(image)
    # 设置内容类型
    resp.headers['Content-Type'] = 'image/jpeg'
    return resp


@passport_bp.route('/smscode', methods=['POST', 'GET'])
def send_sms():
    """
    1. 接收参数并判断是否有值
    2. 校验手机号是正确
    3. 通过传入的图片编码去redis中查询真实的图片验证码内容
    4. 进行验证码内容的比对
    5. 生成发送短信的内容并发送短信
    6. redis中保存短信验证码内容
    7. 返回发送成功的响应
    :return:
    """
    # 1. 接收参数并判断是否有值
    # 取到请求值中的内容
    param_dict = request.json
    mobile = param_dict.get('mobile')
    image_code = param_dict.get('image_code')
    image_code_id = param_dict.get('image_code_id')

    if not all([mobile, image_code, image_code_id]):
        # 参数不全
        return jsonify(errno=RET.PARAMERR, errmsg='参数不全')

    # 2. 验证手机号是否正确
    if not re.match(r'^1[3578][0-9]{9}$', mobile):
        # 提示手机号不正确
        return jsonify(errno=RET.DATAERR, errmsg='手机号不正确')

    # 3. 通过传入的图片编码去 redis 中查询真实的图片验证码内容
    try:
        real_image_code = redis_store.get('ImageCode_' + image_code_id)

        # 如果能取出来值，删除 redis 缓存的内容
        if real_image_code:
            # real_image_code = real_image_code.decode()
            redis_store.delete('ImageCode_' + image_code_id)
    except Exception as e:
        current_app.logger.error(e)
        # 获取图片验证码失败
        return jsonify(errno=RET.DBERR, errmsg='获取图片验证码失败')

    # 3.1 判断验证码是否存在，已过期
    if not real_image_code:
        # 验证码已过期
        return jsonify(errno=RET.NODATA, errmsg='验证码已过期')

    # 4. 进行验证码内容的比对
    if image_code.lower() != real_image_code.lower():
        # 验证码输入错误
        return jsonify(errno=RET.DATAERR, errmsg='验证码输入错误')

    # 4.1 验证该手机是否已经注册
    try:
        user = User.query.filter_by(mobile=mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据库查询错误')
    if user:
        # 该手机已被注册
        return jsonify(errno=RET.DATAEXIST, errmsg='该手机已被注册')

    # 5. 生成发送短信的内容并发送短信
    result = random.randint(0, 999999)
    sms_code = '%06d' % result
    current_app.logger.debug('短信验证码的内容：%s' % sms_code)
    result = CCP().send_template_sms(mobile, [sms_code, constants.SMS_CODE_REDIS_EXPIRES / 60], "1")
    # if result != 0:
    #     # 发送短信失败
    #     return jsonify(errno=RET.THIRDERR, errmsg="发送短信失败")

    # 6. redis 中保存短信验证码内容
    try:
        redis_store.set("SMS_" + mobile, sms_code, constants.SMS_CODE_REDIS_EXPIRES)
    except Exception as e:
        current_app.logger.error(e)
        # 保存短信验证码失败
        return jsonify(errno=RET.DBERR, errmsg="保存短信验证码失败")

    # 7. 返回发送成功的响应
    return jsonify(errno=RET.OK, errmsg="发送成功")


@passport_bp.route('/register', methods=['POST'])
def register():
    """
        1. 获取参数和判断是否有值
        2. 从redis中获取指定手机号对应的短信验证码的
        3. 校验验证码
        4. 初始化 user 模型，并设置数据并添加到数据库
        5. 保存当前用户的状态
        6. 返回注册的结果
        :return:
        """
    # 1. 获取参数和判断是否有值
    json_data = request.json
    mobile = json_data.get('mobile')
    sms_code = json_data.get('smscode')
    password = json_data.get('password')

    if not all([mobile, sms_code, password]):
        # 参数不全
        return jsonify(errno=RET.PARAMERR, errmsg='参数不全')

    # 2. 从 redis 中获取指定手机号对应的短信验证码
    try:
        real_sms_code = redis_store.get('SMS_' + mobile)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='获取本地验证码失败')

    if not real_sms_code:
        # 短信验证码过期
        return jsonify(errno=RET.NODATA, errmsg='短信验证码过期')

    # 3. 校验验证码
    if sms_code != real_sms_code:
        return jsonify(errno=RET.DATAERR, errmsg='短信验证码错误')
    # 删除短信验证码
    try:
        redis_store.delete('SMS_' + mobile)
    except Exception as e:
        current_app.logger.error(e)

    # 4. 初始化 user 模型，并设置数据添加到数据库
    user = User()
    user.nick_name = mobile
    user.mobile = mobile
    # 对密码进行处理
    user.password = password

    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        db.session.rollback()
        current_app.logger.error(e)
        return jsonify(errno=RET.DATAERR, errmsg='数据保存错误')

    # 5. 保存用户登录状态
    session['user_id'] = user.id
    session['nick_name'] = user.nick_name
    session['mobile'] = user.mobile

    # 6. 返回注册结果
    return jsonify(errno=RET.OK, errmsg='OK')


@passport_bp.route('/login', methods=['POST'])
def login():
    """
        1. 获取参数和判断是否有值
        2. 从数据库查询出指定的用户
        3. 校验密码
        4. 保存用户登录状态
        5. 返回结果
        :return:
        """

    # 1. 获取参数和判断是否有值
    json_data = request.json

    mobile = json_data.get('mobile')
    password = json_data.get('password')

    if not all([mobile, password]):
        # 参数不全
        return jsonify(errno=RET.PARAMERR, errmsg='参数不全')

    # 2. 从数据库查询出指定的用户
    try:
        user = User.query.filter_by(mobile=mobile).first()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='查询数据错误')

    if not user:
        return jsonify(errno=RET.USERERR, errmsg='用户不存在')

    # 3. 检验密码
    if not user.check_passowrd(password):
        return jsonify(errno=RET.PWDERR, errmsg='密码错误')

    # 4. 保存用户登录状态
    session['user_id'] = user.id
    session['nick_name'] = user.nick_name
    session['mobile'] = user.mobile
    if user.is_admin:
        session['is_admin'] = True

    # 记录用户最后一次登录时间
    user.last_login = datetime.now()
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)

    # 5. 登录成功
    return jsonify(errno=RET.OK, errmsg='OK')


@passport_bp.route('/logout', methods=['POST'])
def logout():
    """
    清除 session 中的对应登录之后保存的信息
    :return:
    """
    session.pop('user_id', None)
    session.pop('nick_name', None)
    session.pop('mobile', None)
    session.pop('is_admin', None)

    # 返回结果
    return jsonify(errno=RET.OK, errmsg='Ok')
