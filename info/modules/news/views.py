from info import constants, db
from info.models import User, News, Comment, CommentLike
from info.utils.response_code import RET
from . import news_bp
from flask import render_template, session, current_app, g, abort, request, jsonify
from info.utils.common import user_login_data


@news_bp.route('/<int:news_id>')
@user_login_data
def news_detail(news_id):
    """新闻详情首页"""

    # 1 ———— 通过 id 获取用户信息
    # @user_login_data

    # 2 ———— 获取点击排行数据
    news_list = None
    try:
        news_list = News.query.order_by(News.clicks.desc()).limit(constants.CLICK_RANK_MAX_NEWS)
    except Exception as e:
        current_app.logger.error(e)

    click_news_list = []
    for click_news in news_list if news_list else []:
        click_news_list.append(click_news.to_basic_dict())

    # 3 ———— 获取新闻数据
    news = None  # type: News
    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        abort(404)

    if not news:
        # 返回数据未找到的页面
        abort(404)

    news.clicks += 1
    db.session.commit()

    # 4 ———— 判断是否收藏改新闻，默认值为 false
    is_collected = False
    # 判断用户是否关注当前新闻作者
    is_followed = False

    if g.user:
        # 判断用户是否收藏过该新闻
        if news in g.user.collection_news:
            is_collected = True
        # 判断用户是否关注过该新闻的作者
        try:
            if news.user.followers.filter(User.id == g.user.id).count() > 0:
                is_followed = True
        except Exception as e:
            current_app.logger.error(e)

    # 5 ———— 获取当前新闻的评论
    comments = []
    try:
        comments = Comment.query.filter(Comment.news_id == news_id).order_by(Comment.create_time.desc()).all()
    except Exception as e:
        current_app.logger.error(e)

    # comment_list = []
    # for item in comments:  # type: Comment
    #     comment_dict = item.to_dict()
    #     comment_list.append(comment_dict)

    # 6 ———— 点赞评论数据返回
    comment_like_ids = []
    # 如果当前用户已登录
    if g.user:
        try:
            # 所有评论 id
            comment_ids = [comment.id for comment in comments]

            if (len(comment_ids)) > 0:
                # 取到当前用户在当前新闻的所有评论点赞的记录
                comment_likes = CommentLike.query.filter(CommentLike.comment_id.in_(comment_ids),
                                                         CommentLike.user_id == g.user.id).all()

                # 取出记录中所有的评论 id
                comment_like_ids = [comment_like.comment_id for comment_like in comment_likes]
        except Exception as e:
            current_app.logger.error(e)

    comment_list = []
    for item in comments if comments else []:  # type: Comment
        comment_dict = item.to_dict()
        comment_dict['is_like'] = False
        # 判断用户是否点赞评论
        if g.user and item.id in comment_like_ids:
            comment_dict['is_like'] = True
        comment_list.append(comment_dict)

    data = {
        'user_info': g.user.to_dict() if g.user else None,
        'click_news_list': click_news_list,
        'news': news.to_dict(),
        'is_collected': is_collected,
        'comments': comment_list,
        'is_followed': is_followed
    }

    return render_template('news/detail.html', data=data)


@news_bp.route('/news_collect', methods=['POST'])
@user_login_data
def news_collect():
    """新闻收藏"""

    # 1. 参数获取
    user = g.user
    json_data = request.json
    news_id = json_data.get('news_id')
    action = json_data.get('action')

    # 2. 参数校验
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')

    if action not in ('collect', 'cancel_collect'):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='查询数据失败')

    if not news:
        return jsonify(errno=RET.NODATA, errmsg='新闻数据不存在')

    # 3. 逻辑处理
    if action == 'collect':
        user.collection_news.append(news)
    else:
        user.collection_news.remove(news)

    try:
        # db.session.add()
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg='保存失败')

    # 4. 返回数据
    return jsonify(errno=RET.OK, errmsg='操作成功')


@news_bp.route('/news_comment', methods=['POST'])
@user_login_data
def news_comment():
    """添加评论"""

    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')

    # 1. 获取参数
    json_data = request.json
    news_id = json_data.get('news_id')
    comment_str = json_data.get('comment')
    parent_id = json_data.get('parent_id')

    # 2. 校验参数
    if not all([news_id, comment_str]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数不足')

    try:
        news = News.query.get(news_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='查询数据失败')

    if not news:
        return jsonify(errno=RET.NODATA, errmsg='该新闻不存在')

    # 初始化模型，保存数据
    comment = Comment()
    comment.user_id = user.id
    comment.news_id = news_id
    comment.content = comment_str
    if parent_id:
        comment.parent_id = parent_id

    # 保存到数据库
    try:
        db.session.add(comment)
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='保存评论数据失败')

    # 返回响应
    return jsonify(errno=RET.OK, errmsg='评论成功', data=comment.to_dict())


@news_bp.route('/comment_like', methods=['POST'])
@user_login_data
def comment_like():
    """评论点赞/取消点赞"""

    user = g.user
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')

    # 1. 获取参数
    json_data = request.json

    comment_id = json_data.get('comment_id')
    news_id = json_data.get('news_id')
    action = json_data.get('action')  # add, remove

    # 2. 参数校验
    if not all([comment_id, news_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数不足')

    if action not in ('add', 'remove'):
        return jsonify(errno=RET.PARAMERR, errmsg='参数不足')

    # 3. 逻辑处理
    try:
        comment = Comment.query.get(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='查询数据失败')

    if not comment:
        return jsonify(errno=RET.NODATA, errmsg='评论数据不存在')

    if action == 'add':
        # 添加点赞数据
        comment_like = CommentLike.query.filter_by(
            comment_id=comment_id, user_id=user.id).first()
        if not comment_like:
            comment_like = CommentLike()
            comment_like.comment_id = comment_id
            comment_like.user_id = user.id
            db.session.add(comment_like)
            # 添加点赞条数
            comment.like_count += 1
    else:
        # 删除点赞数据
        comment_like = CommentLike.query.filter_by(
            comment_id=comment_id, user_id=user.id).first()
        if comment_like:
            db.session.delete(comment_like)
            # 减少点赞条数
            comment.like_count -= 1

    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        db.session.rollback()
        return jsonify(errno=RET.DBERR, errmsg='操作失败')

    return jsonify(errno=RET.OK, errmsg='操作成功')


@news_bp.route('/followed_user', methods=['POST'])
@user_login_data
def followed_user():
    """关注、取消关注用户"""
    user = g.user  # User
    if not user:
        return jsonify(errno=RET.SESSIONERR, errmsg='用户未登录')

    params = request.json
    user_id = params.get('user_id')
    action = params.get('action')

    if not all([user_id, action]):
        return jsonify(errno=RET.PARAMERR, errmsg='参数不足')

    if action not in ('follow', 'unfollow'):
        return jsonify(errno=RET.PARAMERR, errmsg='参数错误')

    # 查询到关注的用户信息
    try:
        target_user = User.query.get(user_id)
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='查询数据库失败')

    if not target_user:
        return jsonify(errno=RET.NODATA, errmsg='未查询到用户数据')

    # 根据关注、取消关注做不同逻辑
    if action == 'follow':
        if target_user.followers.filter(User.id == user.id).count() > 0:
            return jsonify(errno=RET.DATAEXIST, errmsg='当前已关注')
        target_user.followers.append(g.user)
    else:
        if target_user.followers.filter(User.id == user.id).count() > 0:
            target_user.followers.remove(user)

    # 保存到数据库
    try:
        db.session.commit()
    except Exception as e:
        current_app.logger.error(e)
        return jsonify(errno=RET.DBERR, errmsg='数据保存错误')

    return jsonify(errno=RET.OK, errmsg='操作成功')
