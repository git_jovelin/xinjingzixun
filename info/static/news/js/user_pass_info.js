function getCookie(name) {
    var r = document.cookie.match("\\b" + name + "=([^;]*)\\b");
    return r ? r[1] : undefined;
}


$(function () {
    $(".pass_info").submit(function (e) {
        e.preventDefault();

        // TODO 修改密码
        var params = {}

        // console.info($(this).serializeArray())

        $(this).serializeArray().map(function (x) {
            params[x.name] = x.value
        })

        // 判断两次新密码是否相等
        var new_password = params['new_password']
        var new_password2 = params['new_password2']

        var rePwd = /^\w{6,20}$/;

        if (!rePwd.test(new_password)){
            alert('密码长度为6~20位')
            return
        }

        if (new_password != new_password2){
            alert('两次密码输入不一致')
            return
        }

        $.ajax({
            url: '/user/pass_info',
            type: 'post',
            contentType: 'application/json',
            headers: {
                'X-CSRFToken': getCookie('csrf_token')
            },
            data: JSON.stringify(params),
            success: function (resp) {
                if (resp.errno == '0'){
                    // 修改成功
                    alert('修改成功')
                    // 修改完毕清空输入框内容（当前采用直接刷新当前界面的做法）
                    window.location.reload()
                } else{
                    alert(resp.errmsg)
                }
            }
        })
    })
})