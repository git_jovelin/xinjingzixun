import functools
from flask import session, g, current_app


def do_index_class(index):
    """
    自定义过滤器，过滤点击排序 HTML 的 class
    """
    if index == 0:
        return 'first'
    elif index == 1:
        return 'second'
    elif index == 2:
        return 'third'
    else:
        return ''


def user_login_data(fun_view):
    """获取用户登录信息"""

    @functools.wraps(fun_view)
    def wrapper(*args, **kwargs):
        # 获取到当前登录用户的 id
        user_id = session.get('user_id', None)
        # 通过 id 获取用户信息
        user = None
        if user_id:
            try:
                from info.models import User
                user = User.query.get(user_id)
            except Exception as e:
                current_app.logger.error(e)

        g.user = user

        return fun_view(*args, **kwargs)

    return wrapper
