import redis
import logging


class Config(object):
    """工程配置信息"""
    SECRET_KEY = '9Syw8fhXsRz0ceAdoOb5NNqO60/R8JwTyzG6JjYPMEWIAVwWxf6qnAjMb6gWeo8I'

    # 数据库的配置信息
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@localhost:3306/information'
    # 开启数据库跟踪
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # 请求结束之后进行数据自动提交
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True

    # redis 配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379
    REDIS_NUM = 1

    # Session 的配置信息
    SESSION_TYPE = 'redis'  # 指定 session 保存 redis 中
    SESSION_USE_SINGER = True  # 让 cookie 中的 session_id 被加密
    SESSION_REDIS = redis.StrictRedis(
        host=REDIS_HOST,
        port=REDIS_PORT,
        db=REDIS_NUM)  # 使用 redis 的实例
    # TODO decode_responses=True
    # 注册成功 session 有值时 UnicodeDecodeError: 'utf-8' codec can't decode byte 0xff in position 0: invalid start byte
    SESSION_PERMANENT = False  # True：永久有效，False：设置有过期时间
    PERMANENT_SESSION_LIFETIME = 86400  # session 的有效期，单位为秒

    # 开发模式
    DEBUG = True

    # 默认日志等级
    LOG_LEVEL = logging.DEBUG


class DevelopmentConfig(Config):
    """开发模式下的配置"""
    DEBUG = True

    import info.utils.Colorer

    LOG_LEVEL = logging.DEBUG


class ProductionConfig(Config):
    """生产模式下的配置"""
    # SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@serverip:3306/information'
    # SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEBUG = False

    LOG_LEVEL = logging.WARNING


config_dict = {
    'development': DevelopmentConfig,
    'production': ProductionConfig
}
